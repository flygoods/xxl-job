package com.xxl.job.admin.config;

import groovy.util.logging.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import javax.sql.DataSource;
import java.sql.DatabaseMetaData;

@Configuration
@Slf4j
public class MybatisConfig {

    private static final Logger log = LoggerFactory.getLogger(MybatisConfig.class);

    @Primary
    @Bean
    public SqlSessionFactory sqlSessionFactory(final DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            DatabaseMetaData metaData = dataSource.getConnection().getMetaData();
            String databaseProductName = metaData.getDatabaseProductName().toLowerCase();
            log.info("===>database product name is {}", databaseProductName);
            String xmlPath = "classpath:mybatis-mapper/mysql/*.xml";
            if (databaseProductName.contains("postgresql") || databaseProductName.contains("gauss")) {
                xmlPath = "classpath:mybatis-mapper/gauss/*.xml";
            }
            sqlSessionFactoryBean.setMapperLocations(resolver.getResources(xmlPath));
            return sqlSessionFactoryBean.getObject();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
